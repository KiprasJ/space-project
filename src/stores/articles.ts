import axios from "axios";
import { defineStore } from "pinia";

export const useArticleStore = defineStore("articles", {
  state: () => ({
    articles: [],
    singleArticle: {},
  }),
  getters: {
    getArticles(state) {
      return state.articles;
    },
    getSingleArticle(state) {
      return state.singleArticle;
    },
  },
  actions: {
    async fetchArticles() {
      const response = await axios.get(
        "https://api.spaceflightnewsapi.net/v3/articles"
      );
      this.articles = response.data;
    },
    async fetchSingleArticle(id: any) {
      const response = await axios.get(
        `https://api.spaceflightnewsapi.net/v3/articles/${id}`
      );
      this.singleArticle = response.data;
    },
  },
});
