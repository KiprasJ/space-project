export default function DateFormat(date: String) {
  const getDateOnly = date.split("T");
  return getDateOnly[0];
}
